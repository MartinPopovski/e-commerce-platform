$(function(){

 	// funkcija za fadin na elementi

    $(window).scroll( function(){

     if($(window).width() > 1020){

	   	var positionTop = ($(".fades").offset().top);
	   	var positionBot = ($(".fades").offset().top + $(".fades").outerHeight());

        if($(this).scrollTop() > (positionTop-300)){
	    	
	     $(".hideme").animate({'opacity':'1'},1000);
	    } 
    
     } 
        
    });

     //scrool to div
    $('#scrollToDiv1').click(function() {
            var pos = $('#saati-slika1').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 500);
                return false;
    });
    $('#scrollToDiv2').click(function() {
            var pos = $('#saati-slika2').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 1000);
                return false;
    });
    $('#scrollToDiv3').click(function() {
            var pos = $('#saati-slika3').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 1500);
                return false;
    });
    $('#scrollToDiv4').click(function() {
            var pos = $('#saati-slika4').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 2500);
                return false;
    });
     $('#scrollToDiv5').click(function() {
            var pos = $('#saati-slika5').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 500);
                return false;
    });
    $('#scrollToDiv6').click(function() {
            var pos = $('#saati-slika6').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 1000);
                return false;
    });
    $('#scrollToDiv7').click(function() {
            var pos = $('#saati-slika7').offset().top;
            $('html, body').animate({'scrollTop' : pos-100}, 1500);
                return false;
    });

    


 // dodavanje na saat vo kosnicka

 $(document).on("click", "button.btn-sm.bg-blue.white", function(){
     var imeSaat = $(this).siblings("h4").text();
     var cenaSaat = $(this).siblings("h5").text();
     var slikaSaat = $(this).siblings("img").attr("src");

       // fetchItems();

	 var item = {
	    name: imeSaat,
	    cena: cenaSaat,
	    slika: slikaSaat
     }

     // Test if items is null
  if(localStorage.getItem('items') === null){
    // Init array
    var items = [];
    // Add to array
    items.push(item);
    // Set to localStorage
    localStorage.setItem('items', JSON.stringify(items));
  } else {
    // Get items from localStorage
    var items = JSON.parse(localStorage.getItem('items'));
    // Add bookmark to array
    items.push(item);
    // Re-set back to localStorage
    localStorage.setItem('items', JSON.stringify(items));
  }

  // Clear items
  $("#cart-list").html("");

  // Re-fetch items
  fetchItems();
	 
 });

 
 // Fetch items
 function fetchItems(){
  // Get items from localStorage
  var items = JSON.parse(localStorage.getItem('items'));
  // Get output id
  var itemsResults = document.getElementById('cart-list');

  $(".quantity").html(items.length);

  // Build output
  itemsResults.innerHTML = '';
  for(var i = 0; i < items.length; i++){
    var name = items[i].name;
    var cena = items[i].cena;
    var slika = items[i].slika;


    itemsResults.innerHTML += '<li><div class="well cart-li">'+
    					       '<img style="width: 80px; height: 100px" src="'+slika+'">'+
                               '<span style="margin-right: 20px;">'+name+'</span>'+
                               '<span>'+cena+'</span>'+
                               ' <button class="btn btn-danger" id="remove-from-cart" href="#">x</button> ' +
							   '</div></li>';
  }


}


// remove item

	$(document).on("click", "#remove-from-cart", function(event, e){
		var src = $(this).parent("div").children("img").attr("src");
		console.log(src);
		var items = JSON.parse(localStorage.getItem('items'));

		items.filter(function(val){
				
		if(val.slika === src) {
		var index = items.indexOf(val);
		if (index > -1) {
		  items.splice(index, 1);
		}
		}
		})
		localStorage.setItem('items', JSON.stringify(items));

		$(this).parents("li").remove();
		$(".quantity").html(items.length);

	});

});